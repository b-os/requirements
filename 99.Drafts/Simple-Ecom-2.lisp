;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.

(compay "Symple E-Commerce"
	(state 'Idle
	       "Простой - ожидание действия - корневое состояние")

	(state "Requet for by" :alias 'RFB
	       (ptr "Custumer name")
	       (ptr "PName" :default 0)
	       (ptr "PCount" :default 0)
	       (ptr "Payment Type" :default "Pre-paid")
	       (ptr "Address"))

	(state "Goods is not available" :alias 'GNA
	       (ptr "Available" :default "No"))
	
	(state "Sales is canceled" :alias 'SIC)

	(state "Goods is available" :alias 'GIA
	       (ptr "Available" :default "Yes"))

        (state "Is is prepaid" :alias 'IIP)

	(state "It is post paid" :alias 'IIPPA)

	(state "Bill is ready" :alias 'BIR)

	(state "Bill is not possible" :alias 'BINP)

	(state "Bill is sent" :alias 'BIS)

	(state "Bill isn't sent" :alias 'BINS)

	(state "Bill is paid" :alias 'BIP)

	(state "Biil isn't paid yet" :alias 'BINPY)

	(state "Delivery is requested" :alias 'DIR)

	(state "Order is ready to delivery" :alias 'OIR2D)

	(state "Courier ready to go" :alias 'CR2G)

	(state "Courier picked-up order" :alias 'CPUO)

	(state "Courier on the way" :alias 'COW)

	(state "Courier at eht door" :alias 'CATD)

	(state "Delivery is sucessed" :alias 'DIS)

	(state "Delivery is failed" :alias 'DIF)

	(state "Order is paid" :alias 'OIP)

	(state "Client don't want to pay" :alias 'CNW2P)

	(state "Courier came back" :alias 'CCB)


	(transit "Get request by API"
		 :alias 'T1
		 :from 'Idle

		 (interface :read-from "Orders from WEB"
			    :to (ptr "Order from WEB"))

		 (when (ptr "Order from WEB")
		   (goto 'RFB)))

	(ui-command 'PhoneCall
		    :for 'SalesMan
		    :label "Заказ по телефону")

	(transit "Get request by phone" :alias 'T2 :from 'Idle
		 (when (status 'PhoneCall)
		   (ui-form :for 'SalesMan
			    (field :input :string
				   :label "Customer Name"
				   :to (ptr "Customer Name"))
			    (field :select (from :db "Goods")
				   :label "Product name"
				   :to (ptr "PName"))
			    (field :input :integer
				   :label "Count"
				   :to (ptr "PCount"))
			    (field :select (from :list '("Pre-paid"
							 "Post-paid"))
				   :lable "Type of payment"
				   :to (ptr "PaymentType"))
			    (field :input :string
				   :label "Delivery Address"
				   :to (ptr "Address")))
		   (goto 'RFB)))

	(transit "Check availability and reservation"
		 :alias 'T3
		 :from 'RFB

		 (when (not-empty 'RFB)
		   (when (>= (get-field "Count"
					:from (interface :read-from "DB::Stor"
							 :search (ptr "PName")))
			     (ptr "PCount"))
		     (goto 'GIA))
		   (goto 'RFB)))

	(transit "Message Not Available" :alias 'T4 :from 'GNA
		 (interface :write-to "Message for Client"
			    :message "[PCount] item of [PName] not available right now")
		 (goto 'SIC))

	(transit "Go to Idle" :from 'SIC
		 (goto 'Idle))

	(transit "Check type of payment" :alias 'T5 :from 'GIA
		 (when (eq (ptr "PaymentType") "Pre-paid")
		   (goto 'IIP))

		 (goto 'IIPPA))


	(defun BillRequiest (msg)
	  (interface :write-to "Need Bills" :from msg)
	  (interface :wait-read-from "Make bill for [Customer Name]"))

	(transit "Request the bill" :alias 'T6 :from 'IIP
		 (setf (ptr "Bill")
		       (BillRequiest "Make bill for [Customer Name] - [PCount]x [PName]"))
		 (when (ptr "Bill")
		   (goto 'BIR))

		 (goto 'BINP))
;; etc
	)

				    
