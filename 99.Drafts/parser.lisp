;; <file> ::= {<3pm-statment> | <control-statement>}*
;; <cs-comment> ::= ";" <text> <\n>
;;
;; <control-statement> ::= <cs-comment> | <cs-include>
;;
;; <3pm-statment> ::= <milestone> | <task> | <workpackage> |
;;                    <projec> | <programm> | <portfolio>
;;


;; <common-ptrs> ::== <name> | <id> | <label> | <note>


;; <ms-ptrs> ::= { <indent>
;;                 ( <common-ptrs> | <depend> | <done> | <cost> ) }*
;;
;; <milestone> ::= "Milestone" [<gap>] <:>
;;                 (  [[<gap>] [<name>]]
;;                  | <\n> <inc-indent> <ms-ptrs> <dec-indent> <\n> )



;; <plan-mode-ptr> ::= <fixed-work> | <fixed-duration> | <fixed-assigment> | <default>
;; <plan-ptr> ::= <start> | <duration> | <work> | <finish> | <deadline>
;; <task-assigments> ::= "Assign" [<gap>] <eq> <res-name>
;; <task-flags> ::= <internal> | <billable> | <default>
;; <task-costs> ::= ...
;; <task-ptrs> ::= <indent>
;;                 ( <common-ptr> | <plan-mode-ptrs> | <plan-ptr>
;;                  | <task-assigments | <costs> )
;; <task> ::= "Task" [<gap>] <:>
;;            ( [[<gap>] [<name>]] | <\n> <inc-indent> <task-ptrs> <dec-indent> <\n> )



;; <wp-ptrs> ::= <indent>
;;               ( <common-ptr> | <plan-ptr> | <costs> | <wp-managers> |
;;                 <task> | <milestone> )
;;
;; <wp> ::= "Workpackage" [<gap>] <:>
;;          ( [[<gpa>] [<name>]] 
;;           | <\n> <inc-indent> <wp-ptrs> <dec-indent> <\n> )


;; <prj-ptrs> ::= <indent>
;;                ( <common-ptr> | <prj-baseline> | <prj-costs> | <prj-managers> |
;;                    <wp> | <task> | <milestone> )
;; 
;; <project> ::= "Project" [<gap>] <:>
;;                 ( [[<gap>][<name>]]
;;                  | <\n> <inc-indent> <prj-ptrs> <dec-indent> <\n> )

;; Parser input stream of char, output AST
(defun o3pm-parser (stream)
  (let ((ast ()))

    ast))
